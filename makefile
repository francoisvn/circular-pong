CC =		gcc

CLIBS =		$(shell sdl-config --libs) -lSGE -lSDL_ttf -lfreetype -lSDL_mixer
CFLAGS =	-O3 -Wall $(shell sdl-config --cflags) -g

WINCC =		i586-mingw32msvc-gcc -O3 -Wall
WINCLIBS =		-I/usr/i586-mingw32msvc/include -I/usr/i586-mingw32msvc/include/SDL -D_GNU_SOURCE=1 -Dmain=SDL_main -L/usr/i586-mingw32msvc/lib -lmingw32 -lSDLmain -lSDL -lSGE -lSDL_ttf -lSDL_mixer -mwindows

SOURCES =	circular_pong.c circular_pong.h

all:		circular_pong

clean:
		rm circular_pong

circular_pong:	${SOURCES}
		${CC} ${CFLAGS} -o circular_pong ${SOURCES} ${CLIBS}

win:	${SOURCES}
		${WINCC} -o circular_pong.exe ${SOURCES} ${WINCLIBS}

