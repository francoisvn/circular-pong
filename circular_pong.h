/*
  ------------------------------------------
  circular pong header file
  ------------------------------------------
*/

#ifndef CIRCULAR_PONG_H
#define CIRCULAR_PONG_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "SDL.h"
#include "SDL_thread.h"
#include "sge.h"
#include "SDL_ttf.h"
#include "SDL_mixer.h"

#define PRECISION float
#define HIGHSCORE_NAME 32

struct BALL
{
  PRECISION x;
  PRECISION y;
  PRECISION vx;
  PRECISION vy;
  int remaining;
};
typedef struct BALL BALL;

struct PLAYER
{
  int score;
  PRECISION size;
  PRECISION angle;
  int move;
  SDL_Color color;
  SDLKey keyA;
  SDLKey keyB;
  PRECISION angle_max;
  PRECISION angle_min;
  PRECISION angle_score;
};
typedef struct PLAYER PLAYER;

struct HIGHSCORE
{
  int score;
  char name[HIGHSCORE_NAME];
};
typedef struct HIGHSCORE HIGHSCORE;

void DrawPixel(SDL_Surface *screen,int x,int y,Uint8 R,Uint8 G,Uint8 B);
void Slock(SDL_Surface *screen);
void Sulock(SDL_Surface *screen);
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination );

void store_highscores();
void load_highscores();
int compare_score(HIGHSCORE *hs1,HIGHSCORE *hs2);
int add_score(HIGHSCORE *hs);
void update_score(int lth,int miss);

void do_paddlebounce();
void do_ballmove();
void do_paddlemove();
void do_cycle();

void spawn_ball();

void init_objects_lesser();
void init_objects();
void single_end_game();

void draw_game();
void draw_summary();
void draw_highscores();
void draw_menu();
void draw_set_keys();
void draw_set_name();
void draw_main();

void event_handler(SDL_Event event);
#if EVENT_THREAD
int event_handler_thread(void *data);
#endif

void state_change(int new_state);
void mode_set(int new_mode);

void clean_up();
int inrange(float x,float a,float b);

void DrawPixel(SDL_Surface *screen,int x,int y,Uint8 R,Uint8 G,Uint8 B)
{
  Uint32 color = SDL_MapRGB(screen->format, R, G, B);
  switch (screen->format->BytesPerPixel)
  {
    case 1:
      {
        Uint8 *bufp;
        bufp = (Uint8 *)screen->pixels + y*screen->pitch + x;
        *bufp = color;
      }
      break;
    case 2:
      {
        Uint16 *bufp;
        bufp = (Uint16 *)screen->pixels + y*screen->pitch/2 + x;
        *bufp = color;
      }
      break;
    case 3:
      {
        Uint8 *bufp;
        bufp = (Uint8 *)screen->pixels + y*screen->pitch + x * 3;
        if(SDL_BYTEORDER == SDL_LIL_ENDIAN)
        {
          bufp[0] = color;
          bufp[1] = color >> 8;
          bufp[2] = color >> 16;
        }
        else
        {
          bufp[2] = color;
          bufp[1] = color >> 8;
          bufp[0] = color >> 16;
        }
      }
      break;
    case 4:
      {
        Uint32 *bufp;
        bufp = (Uint32 *)screen->pixels + y*screen->pitch/4 + x;
        *bufp = color;
      }
      break;
  }
}

void Slock(SDL_Surface *screen)
{
  if (SDL_MUSTLOCK(screen))
  {
    if (SDL_LockSurface(screen)<0)
    {
      return;
    }
  }
}

void Sulock(SDL_Surface *screen)
{
  if (SDL_MUSTLOCK(screen))
  {
    SDL_UnlockSurface(screen);
  }
}

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
  SDL_Rect offset;
  
  offset.x = x;
  offset.y = y;
  
  SDL_BlitSurface( source, NULL, destination, &offset );
}

#endif
