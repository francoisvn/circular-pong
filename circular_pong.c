/*
  ------------------------------------------
  circular pong
  ------------------------------------------
  by francois van niekerk
  ------------------------------------------
*/

//------------------------------------------
#include "circular_pong.h"
//------------------------------------------

//------------------------------------------
//-- defines
//------------------------------------------
#define TITLE "circular pong (beta 0.03)"
#define MAX_X 640
#define MAX_Y MAX_X
#define EVENT_THREAD 0
#define MAX_FPS 1000
#define TIME_FACTOR 20

#define DRAW_AA 1
#define DRAW_ARENA 0
#define DRAW_LINES 0
#define DRAW_POINTS 1
#define DRAW_DEBUG 0

#define BALL_N 10
#define BALL_SIZE 5
#define BALL_VEL_SPEEDUP 0.03
#define BALL_VEL_INIT 3
#define BALL_VEL_INC 0.3
#define BALL_VEL_MAX 10

#define ARENA_SIZE 240
#define ARENA_SPACING 0.01
#define ARENA_SCORE 50

#define PLAYER_SPEED 0.03
#define PLAYER_SIZE 0.2
#define PLAYER_SIZE_DEC 0.005
#define PLAYER_SIZE_MIN 0.05
#define PLAYER_THICKNESS 10
#define PLAYER_FRICTION -0.2

#define GO_TIME 3
#define GO_TIME_DEC 0.02

#define POINTS_N 200
#define POINTS_SIZE 2

#define COLLISION_MAX_DIST 150

#define SCORE_SINGLE_HIT 1
#define SCORE_MULTI_HIT 2
#define SCORE_MULTI_MISS -1
#define SCORE_DIFF_DEC 0.01
#define SCORE_DIFF_SIZE 200

#define TEXT_FONT "mgopenmodata.ttf"
#define TEXT_SIZE 28
#define TEXT_SIZE_SMALL 14
#define TEXT_SIZE_LARGE 56
#define TEXT_FPS_ACC 10

#define HIGHSCORE_FILE "highscores.dat"
#define HIGHSCORE_LEN 10

#define SOUND_ON 1
#define SOUND_BOING "boing.ogg"
#define SOUND_POINT "point.ogg"

#define STATE_GAME 1
#define STATE_MENU_MAIN 2
#define STATE_MENU_MULTI 3
#define STATE_SUMMARY 4
#define STATE_SET_KEYS 5
#define STATE_HIGHSCORES 6
#define STATE_SET_NAME 7

#define MODE_SINGLE 1
#define MODE_MULTI 2

#if DRAW_AA
  #define draw_text TTF_RenderText_Blended
  #define draw_circle sge_AAFilledCircle
  #define draw_circle_outline sge_AACircle
  #define draw_line sge_AALine
  #define draw_polygon sge_AAFilledPolygon
#else
  #define draw_text TTF_RenderText_Solid
  #define draw_circle sge_FilledCircle
  #define draw_circle_outline sge_Circle
  #define draw_line sge_Line
  #define draw_polygon sge_FilledPolygon
#endif
//------------------------------------------

//------------------------------------------
//-- global variables
//------------------------------------------
SDL_Surface *screen;
TTF_Font *font;
TTF_Font *font_small;
TTF_Font *font_large;
#if SOUND_ON
  Mix_Chunk *boing;
  Mix_Chunk *point;
#endif

int ticks;
int ticks_delta;
float fps, fps_hist[TEXT_FPS_ACC];

int running;
int pause;
int state;
int mode;
float go;
float score_diff;
int score_multi_hit;
int score_multi_miss;
int set_keys_player;
int set_keys_key;

int players_count;

BALL ball;
PLAYER *players;
int lasttohit;

HIGHSCORE highscores[HIGHSCORE_LEN];
HIGHSCORE current;
int name_pos;
int highscore_pos;
//------------------------------------------

//------------------------------------------
//-- executed when a single game ends
//------------------------------------------
void single_end_game()
{
  load_highscores();
  
  if (players[0].score>highscores[HIGHSCORE_LEN-1].score)
  {
    current.score=players[0].score;
    current.name[0]=0;
    state_change(STATE_SET_NAME);
  }
  else
    state_change(STATE_HIGHSCORES);
}
//------------------------------------------

//------------------------------------------
//-- store highscores in file
//------------------------------------------
void store_highscores()
{
  FILE *fPtr;
  
  if ((fPtr=fopen(HIGHSCORE_FILE,"wb"))==NULL)
    return;
  
  fwrite(&highscores,sizeof(HIGHSCORE),HIGHSCORE_LEN,fPtr);
  
  fclose(fPtr);
}
//------------------------------------------

//------------------------------------------
//-- load highscores from file
//------------------------------------------
void load_highscores()
{
  FILE *fPtr;
  int i;
  
  if ((fPtr=fopen(HIGHSCORE_FILE,"rb"))==NULL)
  {
    for (i=0;i<HIGHSCORE_LEN;i++)
    {
      highscores[i].score=0;
      highscores[i].name[0]=0;
    }
    return;
  }
  
  fread(&highscores,sizeof(HIGHSCORE),HIGHSCORE_LEN,fPtr);
  
  fclose(fPtr);
}
//------------------------------------------

//------------------------------------------
//-- compare two scores
//------------------------------------------
int compare_score(HIGHSCORE *hs1,HIGHSCORE *hs2)
{
  if ((hs1->score)>(hs2->score))
    return 1;
  else
    return 0;
}
//------------------------------------------

//------------------------------------------
//-- add score into the highscore list
//------------------------------------------
int add_score(HIGHSCORE *hs)
{
  int i,j;
  
  for (i=0;i<HIGHSCORE_LEN;i++)
  {
    if (compare_score(hs,&highscores[i]))
    {
      for (j=(HIGHSCORE_LEN-1);j>i;j--)
      {
        highscores[j]=highscores[j-1];
      }
      highscores[i]=*hs;
      return i;
    }
  }
  
  return -1;
}
//------------------------------------------

//------------------------------------------
//-- update player scores and apply scoring
//------------------------------------------
void update_score(int lth,int miss)
{
  if (mode==MODE_MULTI)
  {
    if (lth!=-1 && lth!=miss)
    {
      players[lth].score+=SCORE_MULTI_HIT;
      score_multi_hit=lth;
    }
    else
      score_multi_hit=-1;
    
    if (miss!=-1)
    {
      players[miss].score+=(SCORE_MULTI_MISS);
      if (players[miss].score<0)
        players[miss].score=0;
      score_multi_miss=miss;
    }
    else
      score_multi_miss=-1;
    
    score_diff=1;
  }
}
//------------------------------------------

//------------------------------------------
//-- make ball and paddle collisions
//-- ball is a point moving thru a line
//-- paddle is a line
//-- check for collision of two lines
//-- also check if ball leaves arena
//------------------------------------------
void do_paddlebounce()
{
  int i,col,was_collision,miss;
  PLAYER *p;
  PRECISION dx,dy,dx2,dy2,r,theta;
  PRECISION px1,px2,py1,py2,bx1,bx2,by1,by2;
  PRECISION m1,m2,c1,c2;
  PRECISION vv,vt,pt,nvt;
  PRECISION ix,iy;
  
  dx=ball.x-MAX_X/2;
  dy=ball.y-MAX_Y/2;
  
  r=sqrt(dx*dx+dy*dy);
  theta=atan2(dy,dx);
  
  was_collision=0;
  
  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    
    px1=ARENA_SIZE*cos(p->angle-p->size)+MAX_X/2;
    py1=ARENA_SIZE*sin(p->angle-p->size)+MAX_Y/2;
    bx1=ball.x;
    by1=ball.y;
    
    dx2=bx1-(ARENA_SIZE*cos(p->angle)+MAX_X/2);
    dy2=by1-(ARENA_SIZE*sin(p->angle)+MAX_Y/2);
    if ((dx2*dx2+dy2*dy2)>(COLLISION_MAX_DIST*COLLISION_MAX_DIST))
      continue;
    
    px2=ARENA_SIZE*cos(p->angle+p->size)+MAX_X/2;
    py2=ARENA_SIZE*sin(p->angle+p->size)+MAX_Y/2;
    bx2=ball.x+ball.vx*1.5*ticks_delta/TIME_FACTOR;
    by2=ball.y+ball.vy*1.5*ticks_delta/TIME_FACTOR;
    
    ix=0;
    iy=0;
    
    if ((px2==px1)&&(bx2==bx1))
    {
      col=0;
    }
    else if (px2==px1)
    {
      if (inrange(px1,bx1,bx2))
      {
        ix=px1;
        m1=(by2-by1)/(bx2-bx1);
        iy=m1*ix+by1-m1*bx1;
        col=1;
      }
      else
        col=0;
    }
    else if (bx2==bx1)
    {
      if (inrange(bx1,px1,px2))
      {
        ix=bx1;
        m1=(py2-py1)/(px2-px1);
        iy=m1*ix+py1-m1*px1;
        col=1;
      }
      else
        col=0;
    }
    else
    {
      m1=(py2-py1)/(px2-px1);
      m2=(by2-by1)/(bx2-bx1);
      
      if (m1==m2)
        col=0;
      else
      {
        c1=py1-m1*px1;
        c2=by1-m2*bx1;
        
        ix=(c2-c1)/(m1-m2);
        iy=m1*ix+c1;
        
        if (inrange(ix,px1,px2)&&inrange(ix,bx1,bx2))
          col=1;
        else
          col=0;
      }
      
    }
    
    if (col)
    {
      vt=atan2(ball.vy,ball.vx);
      vv=sqrt(ball.vx*ball.vx+ball.vy*ball.vy)+BALL_VEL_INC;
      if (vv>BALL_VEL_MAX)
      {
        vv=BALL_VEL_MAX;
      }
      
      pt=p->angle+M_PI/2;
      if (pt>M_PI)
        pt-=M_PI*2;
      
      nvt=2*pt-vt-p->move*(PLAYER_FRICTION);
      
      ball.x=ix;
      ball.y=iy;
      
      ball.vx=vv*cos(nvt);
      ball.vy=vv*sin(nvt);
      
      if (mode==MODE_SINGLE)
      {
        p->score+=SCORE_SINGLE_HIT;
      }
      
      #if SOUND_ON
        Mix_PlayChannel(-1,boing,0);
      #endif
      
      lasttohit=i;
      was_collision=1;
      break;
    }
  }
  
  if (was_collision)
  {
    for (i=0;i<players_count;i++)
    {
      p=&players[i];
      
      p->size-=PLAYER_SIZE_DEC;
      if (p->size<PLAYER_SIZE_MIN)
      {
        p->size=PLAYER_SIZE_MIN;
      }
    }
  }
  
  if (r>=ARENA_SIZE+15)
  {
    miss=-1;
    for (i=0;i<players_count;i++)
    {
      p=&players[i];
      if (inrange(theta,p->angle_min,p->angle_max))
      {
        miss=i;
        break;
      }
    }
    update_score(lasttohit,miss);
    #if SOUND_ON
      Mix_PlayChannel(-1,point,0);
    #endif
    spawn_ball();
  }
}
//------------------------------------------

//------------------------------------------
//-- move the ball with its velocity
//------------------------------------------
void do_ballmove()
{
  PRECISION vv,nvv;
  
  vv=sqrt(ball.vx*ball.vx+ball.vy*ball.vy);
  if (vv<BALL_VEL_INIT)
  {
    nvv=vv+BALL_VEL_SPEEDUP;
    ball.vx*=nvv/vv;
    ball.vy*=nvv/vv;
  }
  
  ball.x+=ball.vx*ticks_delta/TIME_FACTOR;
  ball.y+=ball.vy*ticks_delta/TIME_FACTOR;
}
//------------------------------------------

//------------------------------------------
//-- move paddle around the arena
//------------------------------------------
void do_paddlemove()
{
  int i;
  PLAYER *p;
  
  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    
    p->angle+=p->move*PLAYER_SPEED*ticks_delta/TIME_FACTOR;
    
    if (p->angle>p->angle_max)
      p->angle=p->angle_max;
    if (p->angle<p->angle_min)
      p->angle=p->angle_min;
    
    while (p->angle>M_PI)
      p->angle-=M_PI*2;
    while (p->angle<(-M_PI))
      p->angle+=M_PI*2;
  }
}
//------------------------------------------

//------------------------------------------
//-- do a game cycle
//------------------------------------------
void do_cycle()
{
  if (go<=0)
  {
    do_paddlebounce();
    do_ballmove();
  }
  else
  {
    go-=GO_TIME_DEC*ticks_delta/TIME_FACTOR;
    #if SOUND_ON
      if (ceil(go+GO_TIME_DEC)>ceil(go))
        Mix_PlayChannel(-1,point,0);
    #endif
  }
  
  if (score_diff>0)
    score_diff-=SCORE_DIFF_DEC*ticks_delta/TIME_FACTOR;
  
  do_paddlemove();
}
//------------------------------------------

//------------------------------------------
//-- spawn a new ball
//------------------------------------------
void spawn_ball()
{
  int i;
  PLAYER *p;
  PRECISION va;
  
  ball.remaining--;
  if (ball.remaining==0)
  {
    if (mode==MODE_SINGLE)
    {
      single_end_game();
    }
    else
      state_change(STATE_SUMMARY);
  }
  
  ball.x=MAX_X/2;
  ball.y=MAX_Y/2;
  va=rand()/((double)RAND_MAX+1)*M_PI*2;
  ball.vx=0.1*cos(va);
  ball.vy=0.1*sin(va);
  
  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    p->size=PLAYER_SIZE;
  }
  
  lasttohit=-1;
}
//------------------------------------------

//------------------------------------------
//-- init some game objects
//------------------------------------------
void init_objects_lesser()
{
  int i;
  PLAYER *p;
  PRECISION a;
  
  ball.remaining=BALL_N+1;
  spawn_ball();
  
  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    
    p->score=0;
    
    p->size=PLAYER_SIZE;
    p->move=0;
    
    a=M_PI*2/players_count;
    p->angle_max=a*(i+1)-M_PI-ARENA_SPACING;
    p->angle_min=a*(i)-M_PI+ARENA_SPACING;
    
    p->angle_score=a*(i+0.5)-M_PI;
    
    p->angle=rand()/((double)RAND_MAX+1)*(p->angle_max-p->angle_min)+p->angle_min;
  }
  
  if (players_count==1)
  {
    players[0].angle_max=+M_PI;
    players[0].angle_min=-M_PI;
    players[0].angle_score=-M_PI/2;
  }
}
//------------------------------------------

//------------------------------------------
// init all game objects
//------------------------------------------
void init_objects()
{
  int i;
  PLAYER *p;
  
  if (players!=NULL)
    free(players);
  players=malloc(sizeof(PLAYER)*players_count);
  
  init_objects_lesser();
  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    
    switch (i)
    {
      case 0:
        p->color.r=238;
        p->color.g=33;
        p->color.b=12;
        break;
      case 1:
        p->color.r=27;
        p->color.g=34;
        p->color.b=242;
        break;
      case 2:
        p->color.r=26;
        p->color.g=226;
        p->color.b=10;
        break;
      case 3:
        p->color.r=145;
        p->color.g=19;
        p->color.b=239;
        break;
      case 4:
        p->color.r=224;
        p->color.g=220;
        p->color.b=0;
        break;
      case 5:
        p->color.r=244;
        p->color.g=0;
        p->color.b=208;
        break;
      default:
        p->color.r=rand()/((double)RAND_MAX+1)*255;
        p->color.g=rand()/((double)RAND_MAX+1)*255;
        p->color.b=rand()/((double)RAND_MAX+1)*255;
        break;
    }
  }
}
//------------------------------------------

//------------------------------------------
//-- draw game gfx
//------------------------------------------
void draw_game()
{
  int i;
  PLAYER *p;
  PRECISION x1,x2,y1,y2;
  PRECISION vv;
  Sint16 px[4],py[4];
  Uint32 c_black,c_ball;
  SDL_Surface *message;
  SDL_Color textColor={0,0,0};
  char text[256];
  #if DRAW_POINTS
    PRECISION theta;
    int j;
  #endif
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  c_black=SDL_MapRGB(screen->format,0,0,0);
  
  #if DRAW_ARENA
    draw_circle_outline(screen,MAX_X/2,MAX_Y/2,ARENA_SIZE+PLAYER_THICKNESS/2,SDL_MapRGB(screen->format,0,0,255));
  #endif
  
  #if DRAW_LINES
    for (i=0;i<50;i++)
    {
      x1=MAX_X/2;
      y1=MAX_Y/2;
      x2=MAX_X/2+MAX_X*cos(i/50.0*M_PI*2);
      y2=MAX_Y/2+MAX_X*sin(i/50.0*M_PI*2);
      draw_line(screen,x1,y1,x2,y2,SDL_MapRGB(screen->format,0,0,255));
    }
  #endif
  
  #if DRAW_POINTS
    for (i=0;i<POINTS_N;i++)
    {
      theta=i*1.0/POINTS_N*M_PI*2-M_PI;
      x1=MAX_X/2+(ARENA_SIZE+PLAYER_THICKNESS*3/2)*cos(theta);
      y1=MAX_Y/2+(ARENA_SIZE+PLAYER_THICKNESS*3/2)*sin(theta);
      
      for (j=0;j<players_count;j++)
      {
        p=&players[j];
        
        if (inrange(theta,p->angle_min-ARENA_SPACING/2,p->angle_max+ARENA_SPACING/2))
        {
          draw_circle(screen,x1,y1,POINTS_SIZE,SDL_MapRGB(screen->format,p->color.r,p->color.g,p->color.b));
          break;
        }
      }
      
    }
  #endif
  
  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    
    x1=ARENA_SIZE*cos(p->angle-p->size)+MAX_X/2;
    y1=ARENA_SIZE*sin(p->angle-p->size)+MAX_Y/2;
    x2=ARENA_SIZE*cos(p->angle+p->size)+MAX_X/2;
    y2=ARENA_SIZE*sin(p->angle+p->size)+MAX_Y/2;
    
    
    px[0]=round(x1);
    py[0]=round(y1);
    px[1]=round(x2);
    py[1]=round(y2);
    px[2]=round(x2 + PLAYER_THICKNESS*cos(p->angle));
    py[2]=round(y2 + PLAYER_THICKNESS*sin(p->angle));
    px[3]=round(x1 + PLAYER_THICKNESS*cos(p->angle));
    py[3]=round(y1 + PLAYER_THICKNESS*sin(p->angle));
    
    draw_polygon(screen,4,px,py,SDL_MapRGB(screen->format,p->color.r,p->color.g,p->color.b));
    draw_line(screen,px[0],py[0],px[1],py[1],c_black);
    draw_line(screen,px[1],py[1],px[2],py[2],c_black);
    draw_line(screen,px[2],py[2],px[3],py[3],c_black);
    draw_line(screen,px[3],py[3],px[0],py[0],c_black);
  }
  
  if (go<=0)
  {
    if (lasttohit!=-1)
      c_ball=SDL_MapRGB(screen->format,players[lasttohit].color.r,players[lasttohit].color.g,players[lasttohit].color.b);
    else
      c_ball=SDL_MapRGB(screen->format,227,219,159);
    draw_circle(screen,ball.x,ball.y,BALL_SIZE,c_ball);
    draw_circle_outline(screen,ball.x,ball.y,BALL_SIZE,c_black);
    
    vv=sqrt(ball.vx*ball.vx+ball.vy*ball.vy);
    if (vv<BALL_VEL_INIT)
    {
      x1=MAX_X/2-10;
      y1=MAX_Y/2-TEXT_SIZE/2;
      sprintf(text,"%d",ball.remaining);
      message=draw_text(font, text, textColor );
      apply_surface(x1, y1, message, screen );
      SDL_FreeSurface( message );
    }
  }
  else
  {
    x1=MAX_X/2-15;
    y1=MAX_Y/2-TEXT_SIZE_LARGE/2;
    sprintf(text,"%d",(int)ceil(go));
    message=draw_text(font_large, text, textColor );
    apply_surface(x1, y1, message, screen );
    SDL_FreeSurface( message );
  }
  
  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    
    x1=MAX_X/2+(ARENA_SIZE+ARENA_SCORE)*cos(p->angle_score);
    y1=MAX_Y/2+(ARENA_SIZE+ARENA_SCORE)*sin(p->angle_score)-TEXT_SIZE/2;
    sprintf(text,"%d",p->score);
    message=draw_text(font, text, textColor );
    apply_surface(x1, y1, message, screen );
    SDL_FreeSurface( message );
  }
  
  if (mode==MODE_MULTI && score_diff>0)
  {
    if (score_multi_hit!=-1)
    {
      p=&players[score_multi_hit];
      
      x1=MAX_X/2+(SCORE_DIFF_SIZE)*cos(p->angle_score);
      y1=MAX_Y/2+(SCORE_DIFF_SIZE)*sin(p->angle_score)-TEXT_SIZE/2;
      sprintf(text,"%d",SCORE_MULTI_HIT);
      message=draw_text(font, text, textColor );
      apply_surface(x1, y1, message, screen );
      SDL_FreeSurface( message );
    }
    if (score_multi_miss!=-1)
    {
      p=&players[score_multi_miss];
      
      x1=MAX_X/2+(SCORE_DIFF_SIZE)*cos(p->angle_score);
      y1=MAX_Y/2+(SCORE_DIFF_SIZE)*sin(p->angle_score)-TEXT_SIZE/2;
      sprintf(text,"%d",SCORE_MULTI_MISS);
      message=draw_text(font, text, textColor );
      apply_surface(x1, y1, message, screen );
      SDL_FreeSurface( message );
    }
  }
  
  if (pause)
  {
    sprintf(text,"paused");
    message=draw_text(font, text, textColor );
    apply_surface(MAX_X/2-40, MAX_Y/2-TEXT_SIZE/2, message, screen );
    SDL_FreeSurface( message );
    
    sprintf(text,"space:continue esc:exit");
    message=draw_text(font_small, text, textColor );
    apply_surface(MAX_X/2-70, MAX_Y/2+TEXT_SIZE/2+5, message, screen );
    SDL_FreeSurface( message );
  }
  
  #if DRAW_DEBUG
    sprintf(text,"fps:%.1f go:%f",fps,go);
    if (pause)
      strcat(text," (paused)");
    message=draw_text(font_small, text, textColor );
    apply_surface(4, MAX_Y-TEXT_SIZE_SMALL-5, message, screen );
    SDL_FreeSurface( message );
  #endif
}
//------------------------------------------

//------------------------------------------
//-- draw game summary screen
//------------------------------------------
void draw_summary()
{
  int i;
  SDL_Surface *message;
  SDL_Color textColor={0,0,0};
  char text[256];
  PLAYER *p;
  PRECISION x,y;
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  sprintf(text,"game summary");
  message=draw_text(font_large, text, textColor);
  apply_surface(140,100,message,screen);
  SDL_FreeSurface( message );

  for (i=0;i<players_count;i++)
  {
    p=&players[i];
    
    x=250;
    y=200+(TEXT_SIZE+5)*i;
    sprintf(text,"player %d: %d",(i+1),p->score);
    message=draw_text(font, text, p->color);
    apply_surface(x, y, message, screen );
    SDL_FreeSurface( message );
  }
  
  sprintf(text,"space:again esc:back");
  message=draw_text(font, text, textColor);
  apply_surface(190,450,message,screen);
  SDL_FreeSurface( message );
}
//------------------------------------------

//------------------------------------------
//-- draw highscores screen
//------------------------------------------
void draw_highscores()
{
  int i;
  SDL_Surface *message;
  SDL_Color c;
  SDL_Color textColor={0,0,0};
  SDL_Color col_red={238,33,12};
  char text[256];
  HIGHSCORE *h;
  PRECISION x,y;
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  sprintf(text,"highscores");
  message=draw_text(font_large, text, textColor);
  apply_surface(190,100,message,screen);
  SDL_FreeSurface( message );

  for (i=0;i<HIGHSCORE_LEN;i++)
  {
    h=&highscores[i];
    
    if (highscore_pos==i)
      c=col_red;
    else
      c=textColor;
    
    x=50;
    y=190+(TEXT_SIZE+5)*i;
    sprintf(text,"%d:",(i+1));
    message=draw_text(font, text, c);
    apply_surface(x, y, message, screen );
    SDL_FreeSurface( message );
    
    if (strlen(h->name)==0)
      sprintf(text,"<empty>");
    else
      sprintf(text,"%s",h->name);
    message=draw_text(font, text, c);
    apply_surface(x+50, y, message, screen );
    SDL_FreeSurface( message );
    
    sprintf(text,"%02d",h->score);
    message=draw_text(font, text, c);
    apply_surface(x+480, y, message, screen );
    SDL_FreeSurface( message );
  }
  
  sprintf(text,"esc:back");
  message=draw_text(font, text, textColor);
  apply_surface(250,550,message,screen);
  SDL_FreeSurface( message );
}
//------------------------------------------

//------------------------------------------
//-- draw menus
//------------------------------------------
void draw_menu()
{
  SDL_Surface *message;
  SDL_Color textColor={0,0,0};
  char text[256];
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  switch (state)
  {
    case STATE_MENU_MAIN:
      sprintf(text,"circular pong");
      message=draw_text(font_large, text, textColor);
      apply_surface(160,100,message,screen);
      SDL_FreeSurface( message );
      
      sprintf(text,"by francois van niekerk");
      message=draw_text(font_small, text, textColor);
      apply_surface(180,150,message,screen);
      SDL_FreeSurface( message );
      
      sprintf(text,"flash.slash@gmail.com");
      message=draw_text(font_small, text, textColor);
      apply_surface(180,163,message,screen);
      SDL_FreeSurface( message );
      
      sprintf(text,"1: single player");
      message=draw_text(font, text, textColor);
      apply_surface(250,MAX_Y/2-50+(TEXT_SIZE+5)*0,message,screen);
      SDL_FreeSurface( message );
      
      sprintf(text,"2: multiplayer");
      message=draw_text(font, text, textColor);
      apply_surface(250,MAX_Y/2-50+(TEXT_SIZE+5)*1,message,screen);
      SDL_FreeSurface( message );
      
      sprintf(text,"3: highscores");
      message=draw_text(font, text, textColor);
      apply_surface(250,MAX_Y/2-50+(TEXT_SIZE+5)*2,message,screen);
      SDL_FreeSurface( message );
      
      sprintf(text,"4: exit");
      message=draw_text(font, text, textColor);
      apply_surface(250,MAX_Y/2-50+(TEXT_SIZE+5)*3,message,screen);
      SDL_FreeSurface( message );
      
      sprintf(text,"F4: toggle fullscreen");
      message=draw_text(font, text, textColor);
      apply_surface(200,550,message,screen);
      SDL_FreeSurface( message );
      break;
    
    case STATE_MENU_MULTI:
      sprintf(text,"how many players? (2-6)");
      message=draw_text(font, text, textColor);
      apply_surface(150,MAX_Y/2-50,message,screen);
      SDL_FreeSurface( message );
      break;
  }
}
//------------------------------------------

//------------------------------------------
//-- draw key config screen
//------------------------------------------
void draw_set_keys()
{
  SDL_Surface *message;
  SDL_Color textColor={0,0,0};
  char text[256];
  PLAYER *p;
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  sprintf(text,"control setup");
  message=draw_text(font_large, text, textColor);
  apply_surface(160,100,message,screen);
  SDL_FreeSurface( message );
  
  p=&players[set_keys_player];
  
  if (!set_keys_key)
    sprintf(text,"press key A for player %d",(set_keys_player+1));
  else
    sprintf(text,"press key B for player %d",(set_keys_player+1));
  message=draw_text(font, text, p->color);
  apply_surface(170,200,message,screen);
  SDL_FreeSurface( message );
  
  sprintf(text,"esc:cancel");
  message=draw_text(font, text, textColor);
  apply_surface(240,270,message,screen);
  SDL_FreeSurface( message );
}
//------------------------------------------

//------------------------------------------
//-- draw name input screen
//------------------------------------------
void draw_set_name()
{
  SDL_Surface *message;
  SDL_Color textColor={0,0,0};
  char text[256];
  
  SDL_FillRect(screen,NULL,SDL_MapRGB(screen->format,255,255,255));
  
  sprintf(text,"highscore!");
  message=draw_text(font_large, text, textColor);
  apply_surface(220,100,message,screen);
  SDL_FreeSurface( message );
  
  sprintf(text,"name: %s_",current.name);
  message=draw_text(font, text, textColor);
  apply_surface(170,200,message,screen);
  SDL_FreeSurface( message );
}
//------------------------------------------

//------------------------------------------
//-- main draw function
//------------------------------------------
void draw_main()
{
  Slock(screen);
  sge_Update_OFF();
  
  switch (state)
  {
    case STATE_GAME:
      draw_game();
      break;
    case STATE_MENU_MAIN:
    case STATE_MENU_MULTI:
      draw_menu();
      break;
    case STATE_SUMMARY:
      draw_summary();
      break;
    case STATE_SET_KEYS:
      draw_set_keys();
      break;
    case STATE_HIGHSCORES:
      draw_highscores();
      break;
    case STATE_SET_NAME:
      draw_set_name();
      break;
  }
  
  sge_Update_ON();
  Sulock(screen);
  SDL_Flip(screen);
}
//------------------------------------------

//------------------------------------------
//-- event handler
//------------------------------------------
void event_handler(SDL_Event event)
{
  int i;
  PLAYER *p;
  SDLKey key;
  
  switch (state)
  {
    case STATE_GAME:
      switch (event.type)
      {
        case SDL_KEYDOWN:
          key=event.key.keysym.sym;
          switch (key)
          {
            case SDLK_SPACE:
              pause=0;
              break;
            case SDLK_ESCAPE:
              if (pause)
                if (mode==MODE_SINGLE)
                  single_end_game();
                else
                  state_change(STATE_SUMMARY);
              else
                pause=1;
              break;
            default:
              break;
          }
          for (i=0;i<players_count;i++)
          {
            p=&players[i];
            if(key==p->keyA)
            {
              p->move=1;
              break;
            }
            else if(key==p->keyB)
            {
              p->move=-1;
              break;
            }
          }
          break;
        case SDL_KEYUP:
          key=event.key.keysym.sym;
          switch (key)
          {
            default:
              break;
          }
          for (i=0;i<players_count;i++)
          {
            p=&players[i];
            if(key==p->keyA || key==p->keyB)
            {
              p->move=0;
              break;
            }
          }
          break;
      }
      break;
    case STATE_MENU_MAIN:
      switch (event.type)
      {
        case SDL_KEYDOWN:
          key=event.key.keysym.sym;
          switch (key)
          {
            case SDLK_1:
              players_count=1;
              mode_set(MODE_SINGLE);
              init_objects();
              state_change(STATE_SET_KEYS);
              break;
            case SDLK_2:
              state_change(STATE_MENU_MULTI);
              break;
            case SDLK_3:
              state_change(STATE_HIGHSCORES);
              break;
            case SDLK_4:
              running=0;
              break;
            case SDLK_ESCAPE:
              running=0;
              break;
            default:
              break;
          }
          break;
      }
      break;
    case STATE_MENU_MULTI:
      switch (event.type)
      {
        case SDL_KEYDOWN:
          key=event.key.keysym.sym;
          switch (key)
          {
            case SDLK_2:
            case SDLK_3:
            case SDLK_4:
            case SDLK_5:
            case SDLK_6:
              players_count=key-SDLK_2+2;
              mode_set(MODE_MULTI);
              init_objects();
              state_change(STATE_SET_KEYS);
              break;
            case SDLK_ESCAPE:
              state_change(STATE_MENU_MAIN);
              break;
            default:
              break;
          }
          break;
      }
      break;
    case STATE_SUMMARY:
      switch (event.type)
      {
        case SDL_KEYDOWN:
          key=event.key.keysym.sym;
          switch (key)
          {
            case SDLK_SPACE:
              init_objects_lesser();
              state_change(STATE_GAME);
              break;
            case SDLK_ESCAPE:
              state_change(STATE_MENU_MAIN);
              break;
            default:
              break;
          }
          break;
      }
      break;
    case STATE_SET_KEYS:
      switch (event.type)
      {
        case SDL_KEYDOWN:
          key=event.key.keysym.sym;
          switch (key)
          {
            case SDLK_ESCAPE:
              state_change(STATE_MENU_MAIN);
              break;
            default:
              if (!set_keys_key)
              {
                players[set_keys_player].keyA=key;
                set_keys_key=!set_keys_key;
                #if SOUND_ON
                  Mix_PlayChannel(-1,point,0);
                #endif
              }
              else
              {
                players[set_keys_player].keyB=key;
                set_keys_key=!set_keys_key;
                set_keys_player++;
                if (set_keys_player==players_count)
                  state_change(STATE_GAME);
                #if SOUND_ON
                  else
                    Mix_PlayChannel(-1,point,0);
                #endif
              }
              break;
          }
          break;
      }
      break;
    case STATE_SET_NAME:
      switch (event.type)
      {
        case SDL_KEYDOWN:
          key=event.key.keysym.sym;
          switch (key)
          {
            case SDLK_RETURN:
              i=add_score(&current);
              store_highscores();
              state_change(STATE_HIGHSCORES);
              highscore_pos=i;
              break;
            case SDLK_BACKSPACE:
              current.name[strlen(current.name)-1]=0;
              break;
            case SDLK_SPACE:
              if (strlen(current.name)<HIGHSCORE_NAME)
              {
                current.name[strlen(current.name)]=' ';
                current.name[strlen(current.name)+1]=0;
              }
              break;
            default:
              if (strlen(current.name)<HIGHSCORE_NAME && key>=SDLK_a && key<=SDLK_z)
              {
                if (SDL_GetModState()&KMOD_SHIFT)
                  current.name[strlen(current.name)]=(key-SDLK_a+'A');
                else
                  current.name[strlen(current.name)]=(key-SDLK_a+'a');
                current.name[strlen(current.name)+1]=0;
              }
              else if (strlen(current.name)<HIGHSCORE_NAME && key>=SDLK_0 && key<=SDLK_9)
              {
                current.name[strlen(current.name)]=(key-SDLK_0+'0');
                current.name[strlen(current.name)+1]=0;
              }
              break;
          }
          break;
      }
      break;
    case STATE_HIGHSCORES:
      switch (event.type)
      {
        case SDL_KEYDOWN:
          key=event.key.keysym.sym;
          switch (key)
          {
            case SDLK_ESCAPE:
              state_change(STATE_MENU_MAIN);
              break;
            default:
              break;
          }
          break;
      }
      break;
  }
  
  switch (event.type)
  {
    case SDL_KEYDOWN:
      key=event.key.keysym.sym;
      switch (key)
      {
        case SDLK_F4:
          SDL_WM_ToggleFullScreen(screen);
          break;
        default:
          break;
      }
      break;
    case SDL_QUIT:
      running=0;
      break;
  }
}
//------------------------------------------

//------------------------------------------
//-- event handler thread
//------------------------------------------
#if EVENT_THREAD
int event_handler_thread(void *data)
{
  SDL_Event event;
  
  while (running)
  {
    if(SDL_PollEvent(&event))
    {
      event_handler(event);
    }
    else
    {
      SDL_Delay(1);
    }
  }
  
  return 0;
}
#endif
//------------------------------------------

//------------------------------------------
//-- change the state
//------------------------------------------
void state_change(int new_state)
{
  state=new_state;
  switch (state)
  {
    case STATE_GAME:
      pause=0;
      score_diff=0;
      go=GO_TIME;
      break;
    case STATE_SET_KEYS:
      set_keys_player=0;
      set_keys_key=0;
      break;
    case STATE_HIGHSCORES:
      load_highscores();
      highscore_pos=-1;
      break;
    case STATE_SET_NAME:
      name_pos=0;
      break;
  }
  #if SOUND_ON
    Mix_PlayChannel(-1,point,0);
  #endif
}
//------------------------------------------

//------------------------------------------
//-- change mode
//------------------------------------------
void mode_set(int new_mode)
{
  mode=new_mode;
}
//------------------------------------------

//------------------------------------------
//-- main function
//------------------------------------------
int main(int argc, char *argv[])
{
  #if EVENT_THREAD
    SDL_Thread *ev_thread;
  #else
    SDL_Event event;
  #endif
  int tprev=0;
  int i;
  float fps_avg;
  
  if (SDL_Init(SDL_INIT_TIMER|SDL_INIT_VIDEO)<0)
  {
    printf( "Unable to init SDL: %s\n", SDL_GetError());
    exit(1);
  }
  atexit(clean_up);
  
  if(TTF_Init()<0)
  {
    printf( "Unable to init TTF: %s\n", TTF_GetError());
    exit(1);
  }
  
  SDL_WM_SetCaption(TITLE,TITLE);
  
  Uint32 flags = SDL_SWSURFACE|SDL_DOUBLEBUF;
  //SDL_FULLSCREEN SDL_ASYNCBLIT SDL_OPENGL SDL_RESIZABLE
  screen = SDL_SetVideoMode(MAX_X, MAX_Y, 24, flags);
  if (screen == NULL)
  {
    printf( "Unable to set video mode: %s\n", SDL_GetError());
    exit(1);
  }
  
  SDL_ShowCursor(SDL_DISABLE);
  
  font=TTF_OpenFont(TEXT_FONT,TEXT_SIZE);
  font_small=TTF_OpenFont(TEXT_FONT,TEXT_SIZE_SMALL);
  font_large=TTF_OpenFont(TEXT_FONT,TEXT_SIZE_LARGE);
  if (font==NULL||font_small==NULL||font_large==NULL)
  {
    printf( "Unable to open font: %s\n", TTF_GetError());
    exit(1);
  }
  
  #if SOUND_ON
    if(Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
      printf( "Unable to init sound: %s\n",Mix_GetError());
      exit(1);
    }
    
    boing = Mix_LoadWAV(SOUND_BOING);
    point = Mix_LoadWAV(SOUND_POINT);
    if((boing==NULL)||(point==NULL))
    {
      printf( "Unable to load sound: %s\n",Mix_GetError());
      exit(1);
    }
  #endif
  
  srand((int)time(0));
  
  running=1;
  state_change(STATE_MENU_MAIN);
  
  #if EVENT_THREAD
    ev_thread=SDL_CreateThread(event_handler_thread,NULL);
  #endif
  
  fps=0;
  ticks=0;
  while (running)
  {
    ticks=SDL_GetTicks();
    ticks_delta=ticks-tprev;
    
    #if !EVENT_THREAD
      while(SDL_PollEvent(&event))
      {
        event_handler(event);
      }
    #endif
    
    if ((ticks-tprev)>(1000/(MAX_FPS)))
    {
      switch (state)
      {
        case STATE_GAME:
          if (!pause)
            do_cycle();
          break;
      }
      
      fps_avg=0;
      for (i=0;i<(TEXT_FPS_ACC-1);i++)
      {
        fps_hist[i]=fps_hist[i+1];
        fps_avg+=fps_hist[i];
      }
      fps_hist[TEXT_FPS_ACC-1]=(1000)/(ticks-tprev);
      fps_avg+=fps_hist[TEXT_FPS_ACC-1];
      fps=fps_avg/TEXT_FPS_ACC;
      
      draw_main();
      
      tprev=ticks;
    }
    else
    {
      SDL_Delay(1);
    }
  }
  
  #if SOUND_ON
    Mix_PlayChannel(-1,point,0);
    SDL_Delay(10);
  #endif
  
  #if EVENT_THREAD
    SDL_KillThread(ev_thread);
  #endif
  
  //if (SDL_GetError()) {printf("Err: %s\n", SDL_GetError());}
  
  //clean_up();
  
  exit(0);
}
//------------------------------------------

//------------------------------------------
//-- close handles and free memory
//------------------------------------------
void clean_up()
{
  TTF_CloseFont(font);
  TTF_CloseFont(font_small);
  TTF_CloseFont(font_large);
  
  #if SOUND_ON
    Mix_FreeChunk(boing);
    Mix_FreeChunk(point);
    Mix_CloseAudio();
  #endif

  TTF_Quit();
  SDL_Quit();
}
//------------------------------------------

//------------------------------------------
//-- check if a val in range
//------------------------------------------
int inrange(float x,float a,float b)
{
  if ((a<=x&&x<=b)||(b<=x&&x<=a))
    return 1;
  else
    return 0;
}
//------------------------------------------

